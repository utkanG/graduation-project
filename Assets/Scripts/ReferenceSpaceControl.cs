﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReferenceSpaceControl : MonoBehaviour
{
    public static ReferenceSpaceControl Instance;

    [Header("Settings")]
    public double minScale = 1;
    public double maxScale = 10000000000000;
    public float camSpeed = 2;
    public float scrollSpeed = 2;

    [Header("Debug")]
    public DVector3 currentPosition;
    public double currentScale = 25512400;
    public List<AdvancedTransform> advTransforms = new List<AdvancedTransform>();

    [Header("References")]
    public Text currentScaleText;
    public Transform cameraPivot;
    public GameObject currentFocus;

    private float maxScaleLog = 13;
    private Vector3 prevMousePosition;

    [Space(10), SerializeField]
    private AdvancedTransform followTarget;

    [HideInInspector] public UnityEventDouble VisionUpdate;

    private UIControl uIControl;

    private void Awake()
    {
        if(!Instance)
            Instance = this;
        else
            Destroy(gameObject);

        VisionUpdate = new UnityEventDouble();
        uIControl = FindObjectOfType<UIControl>();
    }

    private void Start()
    {
        maxScaleLog = Mathf.Log10((float) currentScale);

        advTransforms.Clear();
        advTransforms.AddRange(FindObjectsOfType<AdvancedTransform>());

        UpdateVision();

        Focus(followTarget);
    }

    private void Update()
    {
        UpdateVision();

        //float horizontalInput = Input.GetAxisRaw("Horizontal");
        //float verticalInput = Input.GetAxisRaw("Vertical");

        if(!Extras.IsMouseOverGameWindow) return;

        if(Input.GetMouseButtonDown(0))
            prevMousePosition = Input.mousePosition;

        Vector3 moveDirection = Vector3.zero; /*Vector3.ProjectOnPlane((cameraPivot.forward * verticalInput + cameraPivot.right * horizontalInput), Vector3.up).normalized;*/

        Vector3 rotationMouseInput = Input.GetMouseButton(0) ? (Input.mousePosition - prevMousePosition) : Vector3.zero;

        cameraPivot.transform.Rotate(new Vector3(-rotationMouseInput.y, rotationMouseInput.x) * Time.unscaledDeltaTime);

        float xAngle = cameraPivot.transform.eulerAngles.x;
        if (xAngle >= 57f && xAngle <= 280f)
        {
            float diffWithUpperLimit = Mathf.Abs(xAngle - 57f);
            float diffWithLowerLimit = Mathf.Abs(xAngle - 280f);

            xAngle = diffWithUpperLimit >= diffWithLowerLimit ? 280f : 57f;
        }

        cameraPivot.transform.rotation = Quaternion.Euler(xAngle, cameraPivot.transform.eulerAngles.y, 0);

        float scrollDelta = Input.mouseScrollDelta.y;
        if(moveDirection == Vector3.zero && Math.Abs(scrollDelta) < .001f)
            return;
        else
        {
            currentScale -= scrollDelta * Math.Pow(10, Math.Log10(currentScale)) * Time.unscaledDeltaTime * scrollSpeed;

            currentPosition += new DVector3(moveDirection) * currentScale * Time.unscaledDeltaTime * camSpeed;
        }

        prevMousePosition = Input.mousePosition;
    }

    private void UpdateVision()
    {
        if(followTarget != null)
        {
            currentPosition = followTarget.position;
        }

        // Clamp the double value
        if(currentScale > maxScale)
            currentScale = maxScale;
        else if(currentScale < minScale)
            currentScale = minScale;
        if(currentPosition.x > maxScale)
            currentPosition.x = maxScale;
        else if(currentPosition.x < -maxScale)
            currentPosition.x = -maxScale;
        if(currentPosition.z > maxScale)
            currentPosition.z = maxScale;
        else if(currentPosition.z < -maxScale)
            currentPosition.z = -maxScale;

        foreach(AdvancedTransform t in advTransforms)
        {
            t.ApplyAdvancedTransform(currentScale, currentPosition);
        }
        
        currentScaleText.text = "Current Scale:\n" + (currentScale/1000).ToString("F3") + " kilometers";

        VisionUpdate.Invoke(currentScale);
    }

    private void PlaceAroundTransform(GameObject prefab, int count, float radius)
    {
        Vector3 offset = Vector3.forward * radius;

        for (int i = 0; i < count; i++)
        {
            Instantiate(prefab, transform.position + offset, Quaternion.identity);
            offset = Quaternion.AngleAxis(360f / count, Vector3.forward) * offset;
        }
    }

    public void SetFollowTarget(AdvancedTransform t)
    {
        followTarget = t;
    }

    public void Focus(AdvancedTransform advTransform)
    {
        currentFocus = advTransform.gameObject;
        minScale = advTransform.scale/2;
        currentScale = advTransform.scale;
        currentPosition = advTransform.position;
        followTarget = advTransform;
    }
}
