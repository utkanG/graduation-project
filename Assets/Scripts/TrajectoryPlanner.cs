﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

public class TrajectoryPlanner : MonoBehaviour
{
	public AdvancedTransform Target;
	public AdvancedTransform Chaser;

    public double TargetAltitude = 419;
    public double DeltaV1ToApply;
    public double DeltaV2ToApply;
    public double TMinus;

    [Header("References")]
    [SerializeField] private ScriptableShipData shipData;
    [SerializeField] private AdvancedTransform  maneuverIndicator;
    [FormerlySerializedAs("issPosIndicator")] [FormerlySerializedAs("rendezvousPosIndicator")] [SerializeField] private AdvancedTransform  targetPosIndicator;
    [FormerlySerializedAs("targetPosAtBurnIndicator")] [SerializeField] private AdvancedTransform  chaserPosIndicator;

    private Orbit targetOrbit;
	private Orbit chaserOrbit;
    private Orbit transferOrbit;

    private AdvancedMass chaserMass;
    private AdvancedMass targetMass;
    private AdvancedMass advMass;
    private AdvancedTransform advTransform;

    private GameObject chaserSim;
    private GameObject targetSim;
    private AdvancedMass simChaserMass;
    private AdvancedMass simTargetMass;
    private bool calculating;
    private SpaceshipBehaviour shipBehaviour;
    
    [Header("UI Settings")]
    [SerializeField] private Text deltaV1Text;
    [SerializeField] private Text deltaV2Text;
    [SerializeField] private Text tMinusText;
    [SerializeField] private InputField targetAltitudeText;
    [SerializeField] private Button actionButton;
    [SerializeField] private GameObject calculatingOverlay;

    public bool targetISS;

    private DVector3[] targetVectorsAfterBurn;

    private double waitTime;
    private double burnTime;
    private double currentWaitTime;
    private bool deltaV1Done;

    [SerializeField]
    private Vector3 initialISSPos;

    private void Awake()
    {
        advMass = GetComponent<AdvancedMass>();
        advTransform = GetComponent<AdvancedTransform>();
        transferOrbit = GetComponent<Orbit>();

        shipBehaviour = FindObjectOfType<SpaceshipBehaviour>();

        chaserMass = Chaser.GetComponent<AdvancedMass>();
        targetMass = Target.GetComponent<AdvancedMass>();

		chaserOrbit  = Chaser.GetComponent<Orbit>();
        targetOrbit = Target.GetComponent<Orbit>();
        
        maneuverIndicator = Instantiate(maneuverIndicator, Vector3.up * 1000, Quaternion.identity);
        chaserPosIndicator = Instantiate(chaserPosIndicator, Vector3.up * 1000, Quaternion.identity);
        targetPosIndicator = Instantiate(targetPosIndicator, Vector3.up * 1000, Quaternion.identity);

        SessionControl.Instance.SessionStartEvent.AddListener(OnSessionStart);
        TimeControl.Instance.SkippedTimeEvent.AddListener(OnSkippedTime);
    }

    private void OnDestroy()
    {
        SessionControl.Instance.SessionStartEvent.RemoveListener(OnSessionStart);
        TimeControl.Instance.SkippedTimeEvent.RemoveListener(OnSkippedTime);
    }

    private void OnSkippedTime(double duration)
    {
        currentWaitTime -= duration;
    }

    private void Start()
    {
        ToggleTransferOrbit(false);

        ResetSimMasses();
    }

    private void Update()
    {
        currentWaitTime -= Time.deltaTime;

        if (waitTime <= 0)
        {
            if (deltaV1Done)
            {
                tMinusText.text = "T+" + (-currentWaitTime).ToString("F0");
            }
        }
        else
        {
            tMinusText.text = "T-" + currentWaitTime.ToString("F0");

            if (currentWaitTime <= 0)
            {
                waitTime = 0;
                deltaV1Done = true;
            }
        }
    }

    private DVector3 GetVectorToTarget()
	{
		return Target.position - Chaser.position;
	}   

	public double GetDistanceToTarget()
	{
		return GetVectorToTarget().magnitude;
	}

    // Jump to end of maneuver as if the burn is complete
    public void ApplyManeuver()
    {
        chaserMass.SetPosition(advMass.position);
        chaserMass.SetVelocity(advMass.velocity);
        targetMass.SetPosition(targetVectorsAfterBurn[0]);
        targetMass.SetVelocity(targetVectorsAfterBurn[1]);
        shipBehaviour.ApplyDeltaV(DeltaV1ToApply);
        TimeControl.Instance.TotalTime += (float)currentWaitTime;
        currentWaitTime = -burnTime;
        //DeactivateIndicators();
    }

    public void Calculate()
    {
        if(targetISS)
            CalculateForRendezvous();
        else
            PlanManeuver();
    }

    private void PlanManeuver()
    {
        advTransform.SetPosition(chaserMass.GetPositionAfter(TMinus));
		advMass.SetVelocity(chaserMass.GetVelocityAfter(TMinus));
        maneuverIndicator.gameObject.SetActive(true);
        gameObject.SetActive(true);
        maneuverIndicator.SetPosition(advTransform.position);

        CalculateRequiredDeltaV(out double v1, out double v2);

        double burnTime = DeltaV1ToApply / shipData.EngineDeltaVPerSecond;
        double totalTime = TMinus + burnTime;

        ProcessBurn(burnTime);

        transferOrbit.UpdateOrbit(advTransform.position, advMass.velocity);

        totalTime += transferOrbit.Period / 2;

        chaserPosIndicator.gameObject.SetActive(true);
        targetPosIndicator.gameObject.SetActive(true);
        chaserPosIndicator.SetPosition(advMass.GetPositionAfter(transferOrbit.Period / 2));
        targetPosIndicator.SetPosition(targetMass.GetPositionAfter(totalTime));
    }

    private async void CalculateForRendezvous()
    {
        if(calculating)
            return;
        
        calculating = true;
        TimeControl.Instance.PauseTime();
        calculatingOverlay.SetActive(true);

        maneuverIndicator.gameObject.SetActive(true);
        gameObject.SetActive(true);

        float timer = .1f;
        while (timer > 0)
        {
            timer -= Time.unscaledDeltaTime;
            await Task.Yield();
        }

        ResetSimMasses();

        waitTime = 0;
        double calculatedWaitTime = 0;

        CalculateRequiredDeltaV(out double v1, out double v2);
        burnTime = v1 / shipData.EngineDeltaVPerSecond;

        float chaser1 = Vector3.SignedAngle(chaserMass.transform.position.normalized, initialISSPos, Vector3.right * 23.5f);
        float target1 = Vector3.SignedAngle(targetMass.transform.position.normalized, initialISSPos, Vector3.right * 23.5f);
        double phase1 = (target1 - chaser1) * MathO.Deg2Rad;

        Debug.Log(chaser1);
        Debug.Log(target1);
        //phase1 *= (chaser1 > target1 ? -1 : 1);
        Debug.Log(phase1);
        // Calculate current angles and phase angle (in radians)
        double chaserAngle1 = chaserOrbit.GetMeanAnomalyFromTrue(chaserOrbit.TrueAnomaly * MathO.Deg2Rad);
        double targetAngle1 = targetOrbit.GetMeanAnomalyFromTrue(targetOrbit.TrueAnomaly * MathO.Deg2Rad);
        double phaseAngle1 = chaserAngle1 - targetAngle1;
        phaseAngle1 = -((phaseAngle1 + Math.PI) % (2 * Math.PI) - Math.PI);
        
        // Find the transfer time and the required lead angle (in radians)
        double transferTime = GetTransferTime();
        double leadAngle = transferTime * targetOrbit.MeanMotion;

        // Final phase angle (in radians) and time to wait
        double phaseAngle2 = Math.PI - leadAngle;
        
        //Debug.Log("chaser " + chaserAngle1 * MathO.Rad2Deg);
        //Debug.Log("target " + targetAngle1 * MathO.Rad2Deg);
        //Debug.Log("phase i " + phaseAngle1 * MathO.Rad2Deg);
        //Debug.Log("phase f " + phaseAngle2 * MathO.Rad2Deg);

        int i = 0; // # of revolutions
        do
        {
            calculatedWaitTime = (phaseAngle2 - phase1 - Math.PI * 2 * i) /
                                 (targetOrbit.MeanMotion - chaserOrbit.MeanMotion);
            // Take the burn time into account and subtract half of it from actual burn time
            calculatedWaitTime -= (burnTime / 2);     

            i++;
        }
        while(calculatedWaitTime < 0);

        // Offset wait time for fine tuning
        calculatedWaitTime -= 100;
        currentWaitTime = calculatedWaitTime;

        waitTime = calculatedWaitTime;
        Debug.Log((phase1*MathO.Rad2Deg).ToString("F2")+" : " + waitTime.ToString("F1"));
        Debug.Log((phaseAngle1*MathO.Rad2Deg).ToString("F2")+" : " + waitTime.ToString("F1"));
        Debug.Log((phaseAngle2*MathO.Rad2Deg).ToString("F2")+" : " + waitTime.ToString("F1"));
       
        // Get exact vectors just before burn
        DVector3 chaserPosition = chaserMass.GetPositionAfter(calculatedWaitTime);
        DVector3 chaserVelocity = chaserMass.GetVelocityAfter(calculatedWaitTime);
        DVector3 targetPosition = targetMass.GetPositionAfter(calculatedWaitTime);
        DVector3 targetVelocity = targetMass.GetVelocityAfter(calculatedWaitTime);

        // Prepare simulation object for target to have target's vectors after burn as well
        simChaserMass.SetPosition(chaserPosition);
        simChaserMass.SetVelocity(chaserVelocity);
        simTargetMass.SetPosition(targetPosition);
        simTargetMass.SetVelocity(targetVelocity);
        
        targetPosIndicator.gameObject.SetActive(true);
        chaserPosIndicator.gameObject.SetActive(true);

        chaserPosIndicator.SetPosition(chaserPosition);
        targetPosIndicator.SetPosition(targetPosition);

        targetVectorsAfterBurn = new DVector3[2];
        targetVectorsAfterBurn[0] = simTargetMass.GetPositionAfter(burnTime);
        targetVectorsAfterBurn[1] = simTargetMass.GetVelocityAfter(burnTime);

        //double totalTime = 0;
        //advMass.SetPosition(simChaserMass.position);
        //advMass.SetVelocity(simChaserMass.velocity);
        //maneuverIndicator.SetPosition(advTransform.position);
        //burnTime = v1 / shipData.EngineDeltaVPerSecond;
        //ProcessBurn(burnTime);
        //currentWaitTime = waitTime;
        //totalTime = waitTime + burnTime;
        //totalTime += transferOrbit.Period / 2;
        //chaserPosIndicator.SetPosition(advMass.GetPositionAfter(transferOrbit.Period / 2));
        //targetPosIndicator.SetPosition(targetMass.GetPositionAfter(totalTime));
        await FineTuneRendezvous(waitTime);

        actionButton.interactable = true;

        calculatingOverlay.SetActive(false);
        calculating = false;
    }

    private async Task FineTuneRendezvous(double tMinus)
    {
        CalculateRequiredDeltaV(out double v1, out double v2);

        DVector3 chaserPosition = simChaserMass.position;
        DVector3 targetPosition = simTargetMass.position;
	    
	    double currentTMinus = tMinus;
	    double step = 10;
        double increment = 1;
        double maxIncrement = 100;

	    double totalTime = 0;
        double burnTime = 0;

	    double distanceToTarget = (targetPosition - chaserPosition).magnitude;
	    double oldDistanceToTarget = 0;

        bool passedTarget = false;
        int switched = 0;
        int iterationCount = 0;
        bool iteratingTowardsApproach = true;

	    while (step > 1)
        {
            if(!calculating)
                return;
            
            // Set sim position/velocity to forwarded time
            simChaserMass.UpdateState(step);
            simTargetMass.UpdateState(step);
            
            // Set position for planner to calculate burn
            advMass.SetPosition(simChaserMass.position);
            advMass.SetVelocity(passedTarget ? -simChaserMass.velocity : simChaserMass.velocity);
            maneuverIndicator.SetPosition(advTransform.position);
		    
            burnTime = v1 / shipData.EngineDeltaVPerSecond;
		    
            ProcessBurn(burnTime);
            transferOrbit.UpdateOrbit(advTransform.position, advMass.velocity);

            chaserPosition = advMass.GetPositionAfter(transferOrbit.Period / 2);
            
            if(passedTarget)
                simTargetMass.SetVelocity(-simTargetMass.velocity);
            targetPosition = simTargetMass.GetPositionAfter(burnTime + transferOrbit.Period / 2);
            if(passedTarget)
                simTargetMass.SetVelocity(-simTargetMass.velocity);
            distanceToTarget = (targetPosition - chaserPosition).magnitude;
            //Debug.Log(distanceToTarget.ToString("F1"));

            chaserPosIndicator.SetPosition(chaserPosition);
            targetPosIndicator.SetPosition(targetPosition);

            if(passedTarget)
                currentTMinus -= step;
            else
                currentTMinus += step;

            //Debug.Log($"Processed Step: {(passedTarget ? -step : step):F2}, T-{currentTMinus:F2}");
            
            if (oldDistanceToTarget != 0 && distanceToTarget > oldDistanceToTarget)
            {
                if (iterationCount == 1)
                    iteratingTowardsApproach = false;

                if (!iteratingTowardsApproach)
                {
                    step *= 2;
                    if(step > 1000)
                        step = 1000;
                    //Debug.Log($"Set Step to {(passedTarget ? -step : step):F2}");
                }
                else
                {
                    //Debug.Log($"Switched direction after Step: {(passedTarget ? -step : step):F2}, " +
                    //          $"Set Step to {((passedTarget ? -step : step)/2):F2}");
                    step /= 2;

                    simChaserMass.SetVelocity(-simChaserMass.velocity);
                    simTargetMass.SetVelocity(-simTargetMass.velocity);

                    passedTarget = !passedTarget;
                    switched += 3;
                }
            }
            else
            {
                iteratingTowardsApproach = true;
                if (switched > 0)
                {
                    switched--;
                }
                else
                {
                    step *= 2;
                    if(step > 1000)
                        step = 1000;
                    //Debug.Log($"Set Step to {(passedTarget ? -step : step):F2}");
                }
            }

            increment *= 2;
            if(increment > maxIncrement)
                increment = maxIncrement;

            oldDistanceToTarget = distanceToTarget;

            await Task.Yield();
            iterationCount++;
        }

        if (passedTarget)
        {
            simChaserMass.SetVelocity(-simChaserMass.velocity);
            simTargetMass.SetVelocity(-simTargetMass.velocity);
        }

        waitTime = currentTMinus;
        currentWaitTime = waitTime;
        totalTime = currentTMinus + burnTime;
        totalTime += transferOrbit.Period / 2;
	    chaserPosIndicator.SetPosition(advMass.GetPositionAfter(transferOrbit.Period / 2));
	    targetPosIndicator.SetPosition(targetMass.GetPositionAfter(totalTime));

        targetVectorsAfterBurn = new DVector3[2];
        targetVectorsAfterBurn[0] = simTargetMass.GetPositionAfter(burnTime);
        targetVectorsAfterBurn[1] = simTargetMass.GetVelocityAfter(burnTime);

        //Debug.Log(
        //    $"Done with distance to target: {(distanceToTarget / 1000):F3} km to target, T-{currentTMinus:F2}. Rendezvous in {totalTime:F2}");
    }

    private void ResetSimMasses()
    {
        if (chaserSim == null) 
            chaserSim = new GameObject("Chaser Sim Obj");
        if (targetSim == null) 
            targetSim = new GameObject("Target Sim Obj");

        Extras.CopyComponent(Chaser, chaserSim);
        simChaserMass = Extras.CopyComponent(chaserMass, chaserSim);
        simChaserMass.enabled = false;
        Extras.CopyComponent(Target, targetSim);
        simTargetMass = Extras.CopyComponent(targetMass, targetSim);
        simTargetMass.enabled = false;
    }

    private void ToggleTransferOrbit(bool toggle)
    {
        transferOrbit.gameObject.SetActive(toggle);
    }

    private void ProcessBurn(double burnTime)
    {
	    while (burnTime > 0)
	    {
		    burnTime -= Time.fixedDeltaTime;

		    double timeInterval = burnTime < Time.fixedDeltaTime ? burnTime : Time.fixedDeltaTime;
            
		    advMass.velocity      += (advMass.StepGravity() + advMass.velocity.normalized * shipData.EngineDeltaVPerSecond) * timeInterval;
		    advTransform.position += advMass.velocity * timeInterval;
	    }
    }

    private void OnSessionStart()
    {
        calculating = false;
        deltaV1Text.text = "ΔvI:?";
        deltaV2Text.text = "ΔvII:?";

        waitTime = -1;
        actionButton.interactable = false;
        deltaV1Done = false;
        tMinusText.text = "T-?";

        DeactivateIndicators();

        targetPosIndicator.gameObject.SetActive(false);
        chaserPosIndicator.gameObject.SetActive(false);
    }

    public void DeactivateIndicators()
    {
        gameObject.SetActive(false);        
        ToggleTransferOrbit(false);
        maneuverIndicator.gameObject.SetActive(false);
        actionButton.interactable = false;
        calculatingOverlay.SetActive(false);
    }

    private double CalculateRequiredDeltaV(out double deltaV1, out double deltaV2)
    {
        double r1 = chaserOrbit.AverageAltitude * 1000 + MathO.EarthRadius;
        double r2 = TargetAltitude * 1000 + MathO.EarthRadius;
        double transferSemiMajor = (chaserOrbit.SemiMajorAxis + r2) / 2;

        double vCurrent = VisViva(r1, chaserOrbit.SemiMajorAxis);
        double vTransferP = VisViva(r1, transferSemiMajor);
        deltaV1 = vTransferP - vCurrent;

        double vTransferA = VisViva(r2, transferSemiMajor);
        double vTarget = VisViva(r2, r2);
        deltaV2 = vTarget - vTransferA;

        DeltaV1ToApply = deltaV1;
        DeltaV2ToApply = deltaV2;
        deltaV1Text.text = "ΔvI:" + deltaV1.ToString("F");
        deltaV2Text.text = "ΔvII:" + deltaV2.ToString("F");;
        return deltaV1 + deltaV2;
    }

    private double GetTransferTime()
    {
        double r2 = TargetAltitude * 1000 + MathO.EarthRadius;
        double transferSemiMajor = (chaserOrbit.SemiMajorAxis + r2) / 2;

        return Math.PI * Math.Sqrt(Math.Pow(transferSemiMajor, 3) / MathO.EarthGravityParam);
    }

    private double VisViva(double radius, double semiMajor)
    {
        return Math.Sqrt(MathO.EarthGravityParam * (2 / radius - 1 / semiMajor));
    }

    public void SetTargetAltitude(string altitude)
    {
        if (float.TryParse(altitude, out float result))
        {
            TargetAltitude = result;
        }
    }
    public void SetTMinus(string tminus)
    {
        if (float.TryParse(tminus, out float result))
        {
            TMinus = result;
        }
    }

    public void ToggleISSTarget(bool toggle)
    {
        if(targetOrbit == null) return;
        targetISS = toggle;
        if (targetISS)
        {
            targetAltitudeText.text = targetOrbit.AverageAltitude.ToString("F");
        }
    }
}