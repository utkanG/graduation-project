﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SessionControl : MonoBehaviour
{
    public static SessionControl Instance;

    public ScriptableOrbitData SpaceshipOrbitData;
    public ScriptableShipData SpaceshipData;
    public ScriptableOrbitData ISSData;

    [SerializeField] private GameObject MenuObject;
    [SerializeField] private GameObject SessionEndPanel;
    [SerializeField] private Text StartText;
    [SerializeField] private Text AltitudeText;
    [SerializeField] private Text AngleText;
    [SerializeField] private Text DeltaVText;

    public UnityEvent SessionStartEvent;

    public bool SessionActive { get; private set; }

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        SessionActive = false;

        UpdatePlaceHolderText();
    }

    private void UpdatePlaceHolderText()
    {
        AltitudeText.text = SpaceshipOrbitData.AverageAltitude.ToString("F1");
        AngleText.text = SpaceshipOrbitData.TrueAnomaly.ToString("F2");
        DeltaVText.text = SpaceshipData.EngineDeltaVPerSecond.ToString("F2");
    }

    public void SetChaserTrueAnomaly(string angle)
    {
        if(float.TryParse(angle, out float result))
            SpaceshipOrbitData.TrueAnomaly = result;
    }

    public void SetChaserAltitude(string angle)
    {
        if(float.TryParse(angle, out float result))
            SpaceshipOrbitData.AverageAltitude = result;
    }

    public void SetChaserDeltaΔv(string Δv)
    {
        if(float.TryParse(Δv, out float result))
            SpaceshipData.EngineDeltaVPerSecond = result;
    }

    public void StartSession()
    {
        SessionActive = true;

        StartText.text = "RESET";
        SessionStartEvent.Invoke();
        MenuObject.SetActive(false);
        SessionEndPanel.SetActive(false);

        UpdatePlaceHolderText();
    }

    public void EndSession()
    {
        SessionActive = false;

        TimeControl.Instance.PauseTime();
        SessionEndPanel.SetActive(true);
        MenuObject.SetActive(true);
    }

    private void Update()
    {
        if(!SessionActive) return;

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            MenuObject.SetActive(!MenuObject.activeInHierarchy);
        }
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
