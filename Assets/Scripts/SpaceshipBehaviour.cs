﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceshipBehaviour : MonoBehaviour
{
    public ScriptableShipData ShipData;
    public bool   RelativeToOrbit = true;
    
    private AdvancedTransform advancedTransform;
    private AdvancedMass advancedMass;

    [Header("Target")]
    [SerializeField]
    private AdvancedTransform targetTransform;
    [SerializeField] private DVector3 dockOffset1 = new DVector3(0,0,-3.0708);
    [SerializeField] private DVector3 dockOffset2 = new DVector3(0,0,-37.4633);
    private AdvancedMass targetMass;
    [SerializeField] private List<ParticleSystem> directionalThrusters;
    [SerializeField] private ParticleSystem mainThruster;
    [SerializeField] private GameObject[] dockingIndicators;
    [SerializeField] private TargetIndicator targetIndicator;
    [SerializeField] private GameObject dockingImage;
    [SerializeField] private Text phaseAngleText;
    [SerializeField]
    private Vector3 initialISSPos;

    public double DeltaVApplied = 0;

    private UIControl uiControl;

    private void Awake()
    {
        advancedTransform = GetComponent<AdvancedTransform>();
        advancedMass = GetComponent<AdvancedMass>();

        targetMass = targetTransform.GetComponent<AdvancedMass>();
        foreach(GameObject dockingIndicator in dockingIndicators)
            dockingIndicator.SetActive(!RelativeToOrbit);

        uiControl = FindObjectOfType<UIControl>();

        SessionControl.Instance.SessionStartEvent.AddListener(OnSessionStart);
    }

    private void OnDestroy()
    {
        SessionControl.Instance.SessionStartEvent.RemoveListener(OnSessionStart);
    }

    private void OnSessionStart()
    {
        foreach (GameObject dockingIndicator in dockingIndicators)
        {
            dockingIndicator.SetActive(false);
        }
        dockingImage.SetActive(false);
        DeltaVApplied = 0;
        RelativeToOrbit = true;
        targetIndicator.gameObject.SetActive(false);
    }

    public void ApplyDeltaV(double amount)
    {
        DeltaVApplied += amount;
        uiControl.UpdateMainDeltaVText(DeltaVApplied);
    }

    public void UpdatePhaseAngle()
    {
        if (ReferenceSpaceControl.Instance.currentFocus != advancedMass.MainMass)
        {
            phaseAngleText.text = "φ: -";
            return;
        }

        float chaser1 = Vector3.SignedAngle(advancedMass.transform.position.normalized, initialISSPos, Vector3.right * 23.5f);
        float target1 = Vector3.SignedAngle(targetMass.transform.position.normalized, initialISSPos, Vector3.right * 23.5f);
        double phase1 = (target1 - chaser1);

        phaseAngleText.text = "φ: " + (-phase1).ToString("F2");
    }

    private void Update()
    {
        if (!SessionControl.Instance.SessionActive)
        {
            return;
        }

        UpdatePhaseAngle();

        double distance = GetDistance();
        double relativeSpeed = GetRelativeSpeed();
        bool targetClose = distance < 10000;
        uiControl.UpdateRelativeSpeedText(relativeSpeed);

        if (targetClose && Input.GetKeyUp(KeyCode.R))
        {
            RelativeToOrbit = false;
            foreach (GameObject dockingIndicator in dockingIndicators)
                dockingIndicator.SetActive(!RelativeToOrbit);
        }

        if (targetClose)
        {
            targetIndicator.gameObject.SetActive(true);
            targetIndicator.UpdateDistanceText(distance);
            targetIndicator.UpdateVelocityText(relativeSpeed);

            if (distance < 5 && CheckRotation(15f))
            {
                dockingImage.SetActive(true);

                if (distance < .5f && CheckRotation())
                {
                    dockingImage.SetActive(false);
                    targetIndicator.gameObject.SetActive(false);
                    ReferenceSpaceControl.Instance.Focus(targetTransform);
                    SessionControl.Instance.EndSession();
                }
            }
        }
        else
        {
            targetIndicator.gameObject.SetActive(false);
        }
    }

    private bool CheckRotation(float tolerance = 5)
    {
        return Vector3.Angle(dockingIndicators[0].transform.rotation.eulerAngles, dockingIndicators[1].transform.rotation.eulerAngles) < tolerance;
    }

    private void FixedUpdate()
    {
        DVector3 moveDirection = DVector3.zero;
        DVector3 rotationDir = DVector3.zero;

        if (RelativeToOrbit)
        {
            DVector3 normalizedVelocity = advancedMass.velocity.normalized;
            
            if(Input.GetKey(KeyCode.W))
            {
                mainThruster.Emit(5);
                moveDirection += normalizedVelocity;
            }
            if (Input.GetKey(KeyCode.S))
            {
                moveDirection -= normalizedVelocity;
            }
        }
        else
        {
            if (Input.GetKey(KeyCode.W))
                moveDirection += new DVector3(transform.forward);
            if(Input.GetKey(KeyCode.S))
                moveDirection -= new DVector3(transform.forward);
        
            if(Input.GetKey(KeyCode.Q))
                moveDirection += new DVector3(transform.up);
            if(Input.GetKey(KeyCode.E))
                moveDirection -= new DVector3(transform.up);
        
            if(Input.GetKey(KeyCode.D))
                moveDirection += new DVector3(transform.right);
            if(Input.GetKey(KeyCode.A))
                moveDirection -= new DVector3(transform.right);
        
            if(Input.GetKey(KeyCode.I))
                rotationDir += DVector3.right;
            if(Input.GetKey(KeyCode.K))
                rotationDir -= DVector3.right;
        
            if(Input.GetKey(KeyCode.U))
                rotationDir += DVector3.forward;
            if (Input.GetKey(KeyCode.O))
                rotationDir -= DVector3.forward;

            if(Input.GetKey(KeyCode.L))
                rotationDir += DVector3.up;
            if(Input.GetKey(KeyCode.J))
                rotationDir -= DVector3.up;
        }

        // Relative Break
        if (Input.GetKey(KeyCode.X))
        {
            rotationDir = -advancedMass.localAngularVelocity;

            if(RelativeToOrbit) 
                return;

            double relativeSpeed = GetRelativeSpeed();
            moveDirection = relativeSpeed * (targetMass.velocity - advancedMass.velocity).normalized;
            if (relativeSpeed < 1)
            {
                advancedMass.SetVelocity(targetMass.velocity);
                moveDirection = DVector3.zero;
            }
        }

        // Move Direction
        if(moveDirection.magnitude != 0)
        {
            double magnitude = Time.fixedDeltaTime *
                               (RelativeToOrbit ? ShipData.EngineDeltaVPerSecond : ShipData.DirectionalDeltaVPerSecond);
            advancedMass.velocity += moveDirection.normalized * magnitude * (GetDistance() < 5 ? .2f : 1);

            DeltaVApplied += magnitude;
            uiControl.UpdateMainDeltaVText(DeltaVApplied);

            foreach (ParticleSystem directionalThruster in directionalThrusters)
            {
                Vector3 lookAt = transform.position - moveDirection.toVector3 * 100;

                directionalThruster.transform.LookAt(lookAt);
                directionalThruster.Emit(1);
            }
        }

        // Rotation
        if (RelativeToOrbit)
        {
            transform.LookAt(advancedMass.velocity.toVector3 + transform.position);
        }
        else if (rotationDir.magnitude != 0)
        {
            advancedMass.localAngularVelocity +=
                rotationDir.normalized * (Time.fixedDeltaTime * (float) ShipData.AngularSpeedPerSecond);
        }
    }

    public double GetRelativeSpeed()
    {
        return (targetMass.velocity - advancedMass.velocity).magnitude;
    }

    public double GetDistance()
    {
        return (targetTransform.position + dockOffset1 - advancedTransform.position + dockOffset2).magnitude;
    }
}
