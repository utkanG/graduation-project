﻿using System;
using UnityEngine;
using static System.Math;
using static MathO;

public class Orbit : MonoBehaviour
{
    public bool OrbitInitialized;
    public bool ShowApsis;

    public ScriptableOrbitData InitialOrbitData;

    [Header("From Initialization")]
    public double AverageAltitude;
    public double Eccentricity; // e
    public double Inclination;  // i
    public double ArgumentOfPeriapsis; // ω
    public double LongitudeOfAscendingNode; // Ω
    public double TrueAnomaly;  // ν

    [Header("Other Parameters")]
    public double Apogee;
    public double Perigee;
    public double SemiMajorAxis; // a
    public double SemiMinorAxis; // b
    public DVector3 EccentricityVector; // E
    public DVector3 SpecificAngularMomentum; // h
    public double Period; // T
    public double MeanMotion; // n
    
    [Header("References")]
    [SerializeField] private Ellipse ellipse;

    private AdvancedTransform advancedTransform;
    private AdvancedMass      advancedMass;
    
    [SerializeField] private AdvancedTransform apsisPrefab;
    // TODO Maybe color these via their material
    private AdvancedTransform apoapsisObj;
    private AdvancedTransform periapsisObj;

    public double CurrentAltitude => (advancedTransform.position.magnitude - EarthRadius) / 1000;

    private void Awake()
    {
        advancedTransform = GetComponent<AdvancedTransform>();
        advancedMass      = GetComponent<AdvancedMass>();

        apoapsisObj = Instantiate(apsisPrefab, Vector3.up * 1000, Quaternion.identity);
        periapsisObj = Instantiate(apsisPrefab, Vector3.up * 1000, Quaternion.identity);
        
        OnSessionStart();
        SessionControl.Instance.SessionStartEvent.AddListener(OnSessionStart);
    }

    private void OnDestroy()
    {
        SessionControl.Instance.SessionStartEvent.RemoveListener(OnSessionStart);
    }

    private void Update()
    {
        if(OrbitInitialized)
            UpdateOrbit(advancedTransform.position, advancedMass.velocity);
    }

    private void InitializeOrbit()
    {
        double tARad = -TrueAnomaly * Deg2Rad; // f
        double loanRad = LongitudeOfAscendingNode * Deg2Rad; // Ω
        double aopRad = ArgumentOfPeriapsis * Deg2Rad; // ω
        double incRad = Inclination * Deg2Rad; // i

        SemiMajorAxis = AverageAltitude * 1000 + EarthRadius;

        double p = SemiMajorAxis * (1 - Eccentricity * Eccentricity); // p = a(1-e^2)
        double r = p / (1 + Eccentricity * Cos(tARad)); // r = p / (1 + e*cos(f))
        double h = Sqrt(p * EarthGravityParam); // p = (h^2)/nu

        DVector3 newPosition = r * new DVector3
        {
            x = Cos(loanRad) * Cos(aopRad + tARad) - 
                Sin(loanRad) * Sin(aopRad + tARad) * Cos(incRad),

            z = -(Sin(loanRad) * Cos(aopRad + tARad) +
                Cos(loanRad) * Sin(aopRad + tARad) * Cos(incRad)),

            y = Sin(incRad) * Sin(aopRad + tARad)
        };

        double nuOverH = EarthGravityParam / h;

        DVector3 newVelocity = new DVector3
        {
            x = -nuOverH * (Cos(loanRad) * (Sin(aopRad + tARad) + Eccentricity * Sin(aopRad)) + 
                            Sin(loanRad) * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Cos(incRad)),
            z = nuOverH * (Sin(loanRad) * (Sin(aopRad + tARad) + Eccentricity * Sin(aopRad)) - 
                            Cos(loanRad) * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Cos(incRad)),
            y = nuOverH * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Sin(incRad)
        };

        advancedTransform.SetPosition(newPosition);
        advancedMass.SetVelocity(-newVelocity);
        
        OrbitInitialized = true;
    }

    public void UpdateOrbit(DVector3 position, DVector3 velocity)
    {
        SemiMajorAxis = EarthGravityParam
                      / (2d * EarthGravityParam / position.magnitude - Pow(velocity.magnitude, 2d));

        MeanMotion = Sqrt(EarthGravityParam / Pow(SemiMajorAxis, 3));

        SpecificAngularMomentum = DVector3.Cross(position, velocity);

        EccentricityVector   = (DVector3.Cross(velocity, SpecificAngularMomentum) / EarthGravityParam) - (position / position.magnitude);
        Eccentricity         = EccentricityVector.magnitude;

        SemiMinorAxis = SemiMajorAxis * Sqrt(1d - Pow(Eccentricity, 2d));

        Period = 2 * PI * Sqrt(Pow(SemiMajorAxis, 3) / EarthGravityParam);

        Apogee  = (1 + Eccentricity) * SemiMajorAxis;
        Perigee = (1 - Eccentricity) * SemiMajorAxis;

        // the z-component corresponds to -y in Unity (!)
        Inclination = Acos(-SpecificAngularMomentum.y / SpecificAngularMomentum.magnitude) * Rad2Deg;

        TrueAnomaly = Acos(DVector3.Dot(EccentricityVector, position) / (Eccentricity * position.magnitude)) * Rad2Deg;
        if (DVector3.Dot(position, velocity) < 0d)
            TrueAnomaly = 360d - TrueAnomaly;

        // n = vector pointing toward ascending node
        DVector3 n = DVector3.Cross(DVector3.up, SpecificAngularMomentum);
        ArgumentOfPeriapsis = 180d - Acos(DVector3.Dot(n, EccentricityVector) / (n.magnitude * EccentricityVector.magnitude)) * Rad2Deg;
        if (EccentricityVector.y < 0d)
            ArgumentOfPeriapsis = 360d - ArgumentOfPeriapsis;

        LongitudeOfAscendingNode = Acos(-n.x / n.magnitude) * Rad2Deg;
        if (LongitudeOfAscendingNode < -n.z)
            LongitudeOfAscendingNode = 360d - LongitudeOfAscendingNode;

        if (ShowApsis)
        {
            apoapsisObj.position = -EccentricityVector.normalized * Apogee;
            periapsisObj.position = EccentricityVector.normalized * Perigee;
        }

        ellipse.ApplyOrbit(this);
    }

    public DVector3 GetPositionFor(double nu)
    {
        double tARad = -nu * Deg2Rad;                // f
        double loanRad = LongitudeOfAscendingNode * Deg2Rad; // Ω
        double aopRad = ArgumentOfPeriapsis * Deg2Rad;       // ω
        double incRad = Inclination * Deg2Rad;               // i

        SemiMajorAxis = AverageAltitude * 1000 + EarthRadius;

        double p = SemiMajorAxis * (1 - Eccentricity * Eccentricity); // p = a(1-e^2)
        double r = p / (1 + Eccentricity * Cos(tARad));               // r = p / (1 + e*cos(f))
        double h = Sqrt(p * EarthGravityParam);                 // p = (h^2)/nu

        DVector3 newPosition = r * new DVector3
        {
            x = Cos(loanRad) * Cos(aopRad + tARad) - 
                Sin(loanRad) * Sin(aopRad + tARad) * Cos(incRad),

            z = -(Sin(loanRad) * Cos(aopRad + tARad) +
                  Cos(loanRad) * Sin(aopRad + tARad) * Cos(incRad)),

            y = Sin(incRad) * Sin(aopRad + tARad)
        };

        return newPosition;
    }
    
    public DVector3 GetVelocityFor(double nu)
    {
        double tARad = -nu * Deg2Rad;                         // f
        double loanRad = LongitudeOfAscendingNode * Deg2Rad; // Ω
        double aopRad = ArgumentOfPeriapsis * Deg2Rad;       // ω
        double incRad = Inclination * Deg2Rad;               // i

        SemiMajorAxis = AverageAltitude * 1000 + EarthRadius;

        double p = SemiMajorAxis * (1 - Eccentricity * Eccentricity); // p = a(1-e^2)
        double r = p / (1 + Eccentricity * Cos(tARad));               // r = p / (1 + e*cos(f))
        double h = Sqrt(p * EarthGravityParam);                 // p = (h^2)/nu

        double nuOverH = EarthGravityParam / h;

        DVector3 newVelocity = new DVector3
        {
            x = -nuOverH * (Cos(loanRad) * (Sin(aopRad + tARad) + Eccentricity * Sin(aopRad)) + 
                            Sin(loanRad) * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Cos(incRad)),
            z = nuOverH * (Sin(loanRad) * (Sin(aopRad + tARad) + Eccentricity * Sin(aopRad)) - 
                           Cos(loanRad) * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Cos(incRad)),
            y = nuOverH * (Cos(aopRad + tARad) + Eccentricity * Cos(aopRad)) * Sin(incRad)
        };

        return newVelocity;
    }

    private void OnEnable()
    {
        ellipse.gameObject.SetActive(true);
    }

    private void OnDisable()
    {
        if(ellipse != null)
            ellipse.gameObject.SetActive(false);
    }

    public double GetMeanAnomalyFromEccentric(double eccentricAnomaly)
    {
        return (eccentricAnomaly - Eccentricity * eccentricAnomaly);
    }

    public double GetEccentricAnomalyFromTrue(double trueAnomaly)
    {
        return Acos((Eccentricity + Cos(trueAnomaly) / (1 + Eccentricity * Cos(trueAnomaly))));
    }

    public double GetEccentricAnomalyFromMean(double meanAnomaly)
    {
        // M = E - e sinE (Iteration)
        // Required for getting the initial Kepler elements from TLE

        throw new NotImplementedException("Eccentric anomaly from Mean anomaly is not implemented.");
    }

    public double GetTrueAnomalyFromEccentric(double eccentricAnomaly)
    {
        double x = Cos(eccentricAnomaly) - Eccentricity;
        double y = Sqrt(1 - Pow(Eccentricity, 2)) * Sin(eccentricAnomaly);

        return Atan2(y, x);
    }

    public double GetMeanAnomalyFromTrue(double trueAnomaly)
    {
        double eccentricAnomaly = GetEccentricAnomalyFromTrue(trueAnomaly);

        if (double.IsNaN(eccentricAnomaly))
        {
            return double.IsNaN(trueAnomaly) ? 0 : PI;
        }
        else
        {
            return trueAnomaly > PI
                ? 2 * PI - GetMeanAnomalyFromEccentric(eccentricAnomaly)
                : GetMeanAnomalyFromEccentric(eccentricAnomaly);
        }
    }

    private void OnSessionStart()
    {
        AverageAltitude = InitialOrbitData.AverageAltitude;
        Eccentricity = InitialOrbitData.Eccentricity;
        Inclination = InitialOrbitData.Inclination;
        ArgumentOfPeriapsis = InitialOrbitData.ArgumentOfPeriapsis;
        LongitudeOfAscendingNode = InitialOrbitData.LongitudeOfAscendingNode;
        TrueAnomaly = InitialOrbitData.TrueAnomaly;
        
        InitializeOrbit();
    }
}