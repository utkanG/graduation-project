﻿using System;
using System.Globalization;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UnityEventDouble : UnityEvent<double> { }

public class TimeControl : MonoBehaviour
{
    public static TimeControl Instance;

    public UnityEventDouble SkippedTimeEvent;

    private float currentTimeScale;
    private DateTime currentTime;

    private UIControl uiControl;
    [SerializeField] private Text timeText;

    public float TotalTime;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);

        SkippedTimeEvent = new UnityEventDouble();

        uiControl = FindObjectOfType<UIControl>();

        OnSessionStart();
        SessionControl.Instance.SessionStartEvent.AddListener(OnSessionStart);
    }

    private void OnDestroy()
    {
        SessionControl.Instance.SessionStartEvent.RemoveListener(OnSessionStart);
    }

    private void Update()
    {
        TotalTime += Time.deltaTime;
        uiControl.UpdateTimeStat(TotalTime);
        currentTime = currentTime.AddSeconds(Time.deltaTime);
        timeText.text = currentTime.ToString(CultureInfo.InvariantCulture);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!SessionControl.Instance.SessionActive)
            {
                return;
            }

            ToggleTimeFlow();
            uiControl.TogglePause();
        }
    }

    public void ToggleTimeFlow()
    {
        if (!SessionControl.Instance.SessionActive)
        {
            return;
        }

        Time.timeScale = Time.timeScale == 0 ? currentTimeScale : 0;
    }

    public void PauseTime()
    {
        Time.timeScale = 0;
        uiControl.TogglePause();
    }

    public void SetTimeScale(float timeScale)
    {
        currentTimeScale = timeScale;
        Time.timeScale = Time.timeScale == 0 ? 0 : timeScale;
    }

    public void MoveForwardInTime(float seconds)
    {
        TotalTime += seconds;
        currentTime = currentTime.AddSeconds(seconds);
        SkippedTimeEvent.Invoke(seconds);
    }

    public void Reset()
    {
        currentTime = DateTime.UtcNow;
        TotalTime = 0;
    }

    private void OnSessionStart()
    {
        currentTimeScale = 1;
        Time.timeScale = 0;

        Reset();
    }
}
