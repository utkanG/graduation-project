﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{
    public Color SelectedColor = Color.green;
    public Color DeselectedColor = Color.white;
    public Color WarningColor = Color.red;

    [Header("References")]
    [SerializeField] private List<Image> timeScaleImages;
    [SerializeField] private Image pauseImage;
    [SerializeField] private Text pauseText;

    [Header("Object Panel")] 
    [SerializeField] private Text totalTimeText;
    [SerializeField] private Text mainDeltaVText;
    [SerializeField] private Text relativeSpeedText;
    
    [Header("End Panel")] 

    private const string DeltaVUnit = " m/s";
    private const string TimeUnit = " s";

    private bool isOrbitSelected;
    private Orbit focusedOrbit;

    private void Awake()
    {
        SessionControl.Instance.SessionStartEvent.AddListener(OnSessionStart);
    }

    private void Start()
    {
        OnSessionStart();
    }

    private void OnDestroy()
    {
        SessionControl.Instance.SessionStartEvent.RemoveListener(OnSessionStart);
    }

    public void UpdateTimeStat(double time)
    {
        totalTimeText.text = time.ToString("F") + TimeUnit;
    }

    public void UpdateMainDeltaVText(double deltaV)
    {
        mainDeltaVText.text = deltaV.ToString("F") + DeltaVUnit;
    }

    public void UpdateRelativeSpeedText(double speed)
    {
        relativeSpeedText.text = speed.ToString("F2") + DeltaVUnit;
    }

    public void TogglePause()
    {
        if (Time.timeScale == 0)
        {
            pauseImage.color = WarningColor;
            pauseText.text = "PAUSED ";
        }
        else
        {
            pauseImage.color = DeselectedColor;
            pauseText.text = "PAUSE ";
        }
    }

    public void SelectTimeScale(int i)
    {
        foreach (Image image in timeScaleImages)
            image.color = DeselectedColor;
        
        timeScaleImages[i].color = SelectedColor;
    }

    public void ChangeFocusedOrbit(GameObject orbitObject)
    {
        if (orbitObject == null)
            isOrbitSelected = false;
        else
        {
            focusedOrbit = orbitObject.GetComponent<Orbit>();
            isOrbitSelected = focusedOrbit != null;
        }
    }

    private void OnSessionStart()
    {
        SelectTimeScale(0);
        TogglePause();

        ChangeFocusedOrbit(null);
        UpdateMainDeltaVText(0);
        UpdateTimeStat(0);
    }
}
