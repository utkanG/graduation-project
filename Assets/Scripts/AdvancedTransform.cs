﻿using UnityEngine;

public class AdvancedTransform : MonoBehaviour
{
    public double scale; //diameter
    public float  maxScaleOnScreen = .025f;
    [Header("Cartesian Coordinates")]
    public DVector3 position;

    public DVector3 rotation;

    public bool swapWithIcon;
    [SerializeField] private GameObject icon;
    [SerializeField] private GameObject meshRenderer;

    private void Awake()
    {
        transform.localRotation = Quaternion.Euler(
            (float) rotation.x,
            (float) rotation.y,
            (float) rotation.z);
    }

    public void ApplyAdvancedTransform(double currentScale, DVector3 currentPosition)
    {
        transform.localScale = Vector3.one * Mathf.Max((float) (scale / currentScale), maxScaleOnScreen);

        transform.position = new Vector3(
            (float) ((position.x - currentPosition.x) / currentScale),
            (float) ((position.y - currentPosition.y) / currentScale),
            (float) ((position.z - currentPosition.z) / currentScale));
        
        if ((float) (scale / currentScale) > maxScaleOnScreen)
        {
            if (!swapWithIcon) return;
            
            icon.SetActive(false);
            meshRenderer.SetActive(true);
        }
        else
        {
            if (!swapWithIcon) return;

            icon.SetActive(true);
            meshRenderer.SetActive(false);
        }
    }

    public void Focus()
    {
        ReferenceSpaceControl.Instance.Focus(this);
    }

    public void SetPosition(DVector3 newPos)
    {
        position = newPos;
    }

    public void SetRotation(DVector3 newRot)
    {
        rotation = newRot;
    }
}
