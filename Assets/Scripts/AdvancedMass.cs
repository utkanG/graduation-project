﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class AdvancedMass : MonoBehaviour
{
    public double Mass; // Kilograms

    private AdvancedTransform  advancedTransform;
    private List<AdvancedMass> dominantMasses;
    
    public  DVector3           velocity;
    public  DVector3           localAngularVelocity;

    public GameObject MainMass => dominantMasses[0].gameObject;

    public DVector3 position
    {
        get => advancedTransform.position;
        private set => advancedTransform.position = value;
    }

    private void Awake()
    {
        advancedTransform = GetComponent<AdvancedTransform>();

        dominantMasses = new List<AdvancedMass>();
        foreach (AdvancedMass advancedMass in FindObjectsOfType<AdvancedMass>())
        {
            if(advancedMass.Mass > 1E+22 && this != advancedMass)
                dominantMasses.Add(advancedMass);
        }
    }

    private void Start()
    {
        TimeControl.Instance.SkippedTimeEvent.AddListener(OnTimeSkipped);
    }

    private void OnDestroy()
    {
        TimeControl.Instance.SkippedTimeEvent.RemoveListener(OnTimeSkipped);
    }

    private void OnTimeSkipped(double duration)
    {
        while (duration > 0)
        {
            duration -= Time.fixedDeltaTime;

            UpdateMass(duration <= Time.fixedDeltaTime ? duration : Time.fixedDeltaTime);
        }
    }

    private void FixedUpdate()
    {
        UpdateMass(Time.fixedDeltaTime);
    }

    private void UpdateMass(double timeInterval)
    {
        foreach (AdvancedMass dominantMass in dominantMasses)
        {
            DVector3 vectorToMass = dominantMass.position - position;
            DVector3 unitVector = vectorToMass.normalized;

            double forceMagnitude = MathO.GravitationalConstant * dominantMass.Mass 
                                    / Math.Pow(vectorToMass.magnitude, 2);

            velocity += forceMagnitude * unitVector * timeInterval;
        }
        
        position += velocity * timeInterval;
        transform.Rotate(localAngularVelocity.toVector3 * Time.fixedDeltaTime);
    }

    public DVector3 GetPositionAfter(double duration)
    {
        DVector3 lastPos = position;
        DVector3 lastVel = velocity;

        while (duration > 0)
        {
            duration -= Time.fixedDeltaTime;
            double interval = duration <= Time.fixedDeltaTime ? duration : Time.fixedDeltaTime;

            foreach (AdvancedMass dominantMass in dominantMasses)
            {
                DVector3 vectorToMass = dominantMass.position - lastPos;
                DVector3 unitVector = vectorToMass.normalized;

                double forceMagnitude = MathO.GravitationalConstant * dominantMass.Mass 
                                      / Math.Pow(vectorToMass.magnitude, 2);

                lastVel += forceMagnitude * unitVector * interval;
            }
            
            lastPos += lastVel * interval;
        }

        return lastPos;
    }

    public void UpdateState(double duration)
    {
        while (duration > 0)
        {
            duration -= Time.fixedDeltaTime;
            double interval = duration <= Time.fixedDeltaTime ? duration : Time.fixedDeltaTime;

            foreach (AdvancedMass dominantMass in dominantMasses)
            {
                DVector3 vectorToMass = dominantMass.position - position;
                DVector3 unitVector = vectorToMass.normalized;

                double forceMagnitude = MathO.GravitationalConstant * dominantMass.Mass 
                                        / Math.Pow(vectorToMass.magnitude, 2);

                velocity += forceMagnitude * unitVector * interval;
            }
            
            position += velocity * interval;
        }
    }

    public DVector3 GetVelocityAfter(double duration)
    {
        DVector3 lastPos = position;
        DVector3 lastVel = velocity;

        while (duration > 0)
        {
            duration -= Time.fixedDeltaTime;
            double interval = duration <= Time.fixedDeltaTime ? duration : Time.fixedDeltaTime;

            foreach (AdvancedMass dominantMass in dominantMasses)
            {
                DVector3 vectorToMass = dominantMass.position - lastPos;
                DVector3 unitVector = vectorToMass.normalized;

                double forceMagnitude = MathO.GravitationalConstant * dominantMass.Mass 
                                        / Math.Pow(vectorToMass.magnitude, 2);

                lastVel += forceMagnitude * unitVector * interval;
            }
            
            lastPos += lastVel * interval;
        }

        return lastVel;
    }

    public DVector3 StepGravity()
    { 
        DVector3 gravityResultant = DVector3.zero;

        foreach (AdvancedMass dominantMass in dominantMasses)
        {
            DVector3 vectorToMass = dominantMass.position - position;
            DVector3 unitVector = vectorToMass.normalized;

            double forceMagnitude = MathO.GravitationalConstant * dominantMass.Mass 
                                    / Math.Pow(vectorToMass.magnitude, 2);

            gravityResultant += forceMagnitude * unitVector;
        }

        return gravityResultant;
    }

    public void SetPosition(DVector3 newPos)
    {
        position = newPos;
    }

    public void SetVelocity(DVector3 newVel)
    {
        velocity = newVel;
    }
    
    public void SetAngularVelocity(DVector3 newVel)
    {
        localAngularVelocity = newVel;
    }
}
