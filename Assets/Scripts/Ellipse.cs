﻿using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

[RequireComponent(typeof(LineRenderer))]
public class Ellipse : MonoBehaviour
{
    public int Segments = 64;

    public float XRadius = 1;
    public float ZRadius = 1;

    public float Angle = 0;

    public Color Color = Color.green;
    private Color ClearVersionColor;

    private LineRenderer lineRenderer;
    private AdvancedTransform advancedTransform;

    private void Awake()
    {
        advancedTransform = GetComponent<AdvancedTransform>();
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        ClearVersionColor = new Color(Color.r, Color.g, Color.b, .1f);
    }

    private void Start()
    {
        ReferenceSpaceControl.Instance.VisionUpdate.AddListener(OnVisionUpdate);
    }

    private void OnDestroy()
    {
        ReferenceSpaceControl.Instance.VisionUpdate.RemoveListener(OnVisionUpdate);
    }

    private void OnVisionUpdate(double scale)
    {
        lineRenderer.startColor = scale < 50000 ? Color.clear : Color.Lerp(Color.clear, Color, (float) scale / 250000f);
        lineRenderer.endColor = scale < 50000 ? Color.clear : Color.Lerp(Color.clear, Color, (float) scale / 250000f);
        
        DrawEllipse();
    }

    private void DrawEllipse()
    {
        lineRenderer.positionCount = Segments + 1;

        float angle = Angle; // TODO Depends on other stuff (for different colors from peri to apo)
        Quaternion anomalyRot = Quaternion.AngleAxis(Angle, Vector3.up);
       
        for (int i = 0; i < (Segments + 1); i++)
        {
            float x = Mathf.Sin (Mathf.Deg2Rad * angle) * XRadius;
            float y = 0f;
            float z = Mathf.Cos (Mathf.Deg2Rad * angle) * ZRadius;
            
            lineRenderer.SetPosition (i, anomalyRot * new Vector3(x, y, z));
                   
            angle += (360f / Segments);
        }
    }

    public void ApplyOrbit(Orbit orbit)
    {
        advancedTransform.position = orbit.EccentricityVector.normalized * (orbit.Perigee - orbit.SemiMajorAxis);
        advancedTransform.scale    = orbit.SemiMinorAxis;
        
        XRadius = (float) orbit.SemiMajorAxis / (float) orbit.SemiMinorAxis;
        Angle = (float) orbit.ArgumentOfPeriapsis;
        transform.rotation = Quaternion.Euler(-(float) (orbit.Inclination), (float) orbit.LongitudeOfAscendingNode - 180f, 0);
    }

    private void OnValidate()
    {
        if(!lineRenderer) return;

        DrawEllipse();
    }
}