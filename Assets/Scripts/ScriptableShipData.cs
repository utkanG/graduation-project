﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Ship Data", menuName = "Ship Data", order = 1)]
public class ScriptableShipData : ScriptableObject
{
    public double DirectionalDeltaVPerSecond = 3f;
    public double EngineDeltaVPerSecond = 5f;
    public double AngularSpeedPerSecond = 15f;
}