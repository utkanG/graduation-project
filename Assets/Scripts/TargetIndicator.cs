﻿using UnityEngine;
using UnityEngine.UI;

public class TargetIndicator : MonoBehaviour
{
    public Transform target;

    private Camera mainCamera;
    private RectTransform rectTransform;
    private Text distanceText;
    private Text velocityText;

    void Awake()
    {
        mainCamera = Camera.main;
        rectTransform = GetComponent<RectTransform>();
        distanceText = transform.GetChild(1).GetComponent<Text>();
        velocityText = transform.GetChild(2).GetComponent<Text>();
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        Vector3 viewPortPos = mainCamera.WorldToViewportPoint(target.position);

        rectTransform.anchorMin = viewPortPos;
        rectTransform.anchorMax = viewPortPos;
    }

    public void UpdateDistanceText(double distance)
    {
        distanceText.text = distance.ToString("F") + " m";
    }
    public void UpdateVelocityText(double vel)
    {
        velocityText.text = vel.ToString("F") + " m/s";
    }
}
