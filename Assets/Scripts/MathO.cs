﻿using System;

public static class MathO
{
    /// <summary>
    /// G: m3 * kg-1 * s-2
    /// </summary>
    public const double GravitationalConstant = 6.67408E-11; 

    /// <summary>
    /// // Re: m
    /// </summary>
    public const double EarthRadius = 6371000; 

    /// <summary>
    /// μ: m3 * s-2
    /// </summary>
    public const double EarthGravityParam = 3.986004418E14;

    /// <summary>
    /// M: kg
    /// </summary>
    public const double EarthMass = 5.972E24; 
    
    public static double Clamp(double value, double min, double max)
    {
        if (value < min)
            value = min;
        else if (value > max)
            value = max;
        return value;
    }

    public const double Deg2Rad = Math.PI/180;
    public const double Rad2Deg = 180/Math.PI;
}
