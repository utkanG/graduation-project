﻿using System;
using Vector3 = UnityEngine.Vector3;

[Serializable]
public struct DVector3
{
    public double x;
    public double y;
    public double z;

    public double magnitude => GetMagnitude(this);
    public double sqrMagnitude => GetSqrMagnitude(this);
    public DVector3 normalized => this / magnitude;
    public Vector3 toVector3 => new Vector3((float)x, (float)y, (float)z);
    public static DVector3 zero => new DVector3(0, 0, 0);
    public static DVector3 one => new DVector3(1,  1, 1);
    public static DVector3 up => new DVector3(0,   1, 0);
    public static DVector3 right => new DVector3(1,   0, 0);
    public static DVector3 forward => new DVector3(0,   0, 1);

    public DVector3(double x, double y)
    {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    public DVector3(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public DVector3(Vector3 v)
    {
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }

    public static DVector3 operator +(DVector3 a) => a;
    public static DVector3 operator -(DVector3 a) => new DVector3(-a.x, -a.y, -a.z);
    public static DVector3 operator +(DVector3 a, DVector3 b) => new DVector3(a.x + b.x, a.y + b.y, a.z + b.z);
    public static DVector3 operator -(DVector3 a, DVector3 b) => a + (-b);
    public static DVector3 operator *(double d, DVector3 a) => new DVector3(a.x * d, a.y * d, a.z * d);
    public static DVector3 operator *(DVector3 a, double d) => new DVector3(a.x * d, a.y * d, a.z * d);
    public static DVector3 operator /(double d, DVector3 a) => new DVector3(d / a.x, d / a.y, d / a.z);
    public static DVector3 operator /(DVector3 a, double d) => new DVector3(a.x / d, a.y / d, a.z / d);

    public static double GetMagnitude(DVector3 v)
    {
        return Math.Sqrt((Math.Pow(v.x, 2) + Math.Pow(v.y, 2) + Math.Pow(v.z, 2)));
    }
    
    public static double GetSqrMagnitude(DVector3 v)
    {
        return (Math.Pow(v.x, 2) + Math.Pow(v.y, 2) + Math.Pow(v.z, 2));
    }

    public static DVector3 Cross(DVector3 v1,  DVector3 v2) => new DVector3((v1.y * v2.z - v1.z * v2.y), (v1.z * v2.x - v1.x * v2.z), (v1.x * v2.y - v1.y * v2.x));
    public static double Dot(DVector3 v1, DVector3 v2) => v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
    public static double Angle(DVector3 from, DVector3 to)
    {
        double num = Math.Sqrt(from.sqrMagnitude * to.sqrMagnitude);
        return num < 1.0000000036274937E-15 ? 0.0f : (float) Math.Acos(MathO.Clamp(DVector3.Dot(from, to) / num, -1f, 1f)) * 57.29578f;
    }
}