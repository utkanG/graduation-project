﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Orbit Data", menuName = "Orbit Data", order = 1)]
public class ScriptableOrbitData : ScriptableObject
{
    public double AverageAltitude;
    public double Eccentricity; // e
    public double Inclination;  // i
    public double ArgumentOfPeriapsis; // ω
    public double LongitudeOfAscendingNode; // Ω
    public double TrueAnomaly;  // ν
}
